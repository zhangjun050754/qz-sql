package com.qz.sql.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import com.qz.sql.api.parse.ParseSqlUtils;
import com.qz.sql.editor.entity.DbFavorite;
import com.qz.sql.editor.entity.DbHistory;
import com.qz.sql.editor.json.DocDbResponseJson;
import com.qz.sql.editor.json.ResponseJson;
import com.qz.sql.editor.mapper.base.ExecuteParam;
import com.qz.sql.editor.mapper.base.ExecuteResult;
import com.qz.sql.editor.mapper.base.ExecuteType;
import com.qz.sql.editor.mapper.base.SqlExecutor;
import com.qz.sql.editor.service.DbFavoriteService;
import com.qz.sql.editor.service.DbHistoryService;
import com.qz.sql.editor.utils.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * sql执行器
 *
 * @author
 * @since
 */
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/qz-sql/executor")
public class DbSqlExecutorController {
	private static Logger logger = LoggerFactory.getLogger(DbSqlExecutorController.class);
	
	@Resource
	SqlExecutor sqlExecutor;
	@Resource
	DbHistoryService dbHistoryService;
	@Resource
	DbFavoriteService dbFavoriteService;

	private ParseSqlUtils parseSqlUtils = new ParseSqlUtils();

	@PostMapping(value = "/getTableAliasMap")
	public ResponseJson getTableAliasMap(String sql, String driverType) {
		return new DocDbResponseJson(parseSqlUtils.getTableAliasMap(sql, driverType));
	}

	@PostMapping(value = "/execute")
	public ResponseJson execute(Long sourceId, String executeId, String sql, String params) {
		if (StringUtils.isBlank(sql)) {
			return DocDbResponseJson.warn("执行的SQL不能为空");
		}
		// 保留历史记录
		dbHistoryService.saveHistory(sql.trim(), sourceId);
		List<String> resultList = new LinkedList<>();
		// 支持;分割的多个sql执行
		String[] sqlArr = sql.split(";");
		for (String sqlItem : sqlArr) {
			if (StringUtils.isBlank(sqlItem)) {
				continue;
			}
			sqlItem = sqlItem.trim();
			try {
				ExecuteType executeType = ExecuteType.ALL;
				ExecuteParam executeParam = new ExecuteParam();
				executeParam.setDatasourceId(sourceId);
				executeParam.setExecuteId(executeId);
				executeParam.setExecuteType(executeType);
				executeParam.setSql(sqlItem);
				executeParam.setMaxRows(1000);
				ExecuteResult executeResult = sqlExecutor.execute(executeParam);
				SerializeConfig mapping = new SerializeConfig();
				mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
				mapping.put(Timestamp.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
				String resultJsonStr = JSON.toJSONString(executeResult, mapping, SerializerFeature.WriteMapNullValue);
				resultList.add(resultJsonStr);
			} catch (Exception e) {
				logger.error("执行出错", e);
				ExecuteResult executeResult = ExecuteResult.error(e.getMessage().replaceAll("com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: ",""), sqlItem);
				resultList.add(JSON.toJSONString(executeResult));
			}
		}
		return DocDbResponseJson.ok(resultList);
	}
	
	@PostMapping(value = "/cancel")
	public ResponseJson cancel(String executeId) {
		sqlExecutor.cancel(executeId);
		return DocDbResponseJson.ok();
	}
	
	@PostMapping(value = "/history/list")
	public ResponseJson historyList(Long sourceId) {
		UpdateWrapper<DbHistory> wrapper = new UpdateWrapper<>();
		wrapper.eq(sourceId != null, "datasource_id", sourceId);
		wrapper.orderByDesc("id");
		List<DbHistory> favoriteList = dbHistoryService.list(wrapper);
		return DocDbResponseJson.ok(favoriteList);
	}

	@PostMapping(value = "/favorite/list")
	public ResponseJson favoriteList(Long sourceId) {
		UpdateWrapper<DbFavorite> wrapper = new UpdateWrapper<>();
		wrapper.eq(sourceId != null, "datasource_id", sourceId);
		wrapper.eq("create_user_id", 1);
		wrapper.eq("yn", 1);
		wrapper.orderByDesc("id");
		List<DbFavorite> favoriteList = dbFavoriteService.list(wrapper);
		return DocDbResponseJson.ok(favoriteList);

	}
	
	@PostMapping(value = "/favorite/add")
	public ResponseJson addFavorite(DbFavorite dbFavorite) {
		Integer yn = Optional.ofNullable(dbFavorite.getYn()).orElse(1);
		if (yn == 1) {
			if (StringUtils.isBlank(dbFavorite.getContent())) {
				return DocDbResponseJson.warn("收藏的SQL不能为空");
			}
			dbFavorite.setContent(dbFavorite.getContent().trim());
		}
		if (dbFavorite.getId() != null && dbFavorite.getId() > 0) {
			dbFavoriteService.updateById(dbFavorite);
		} else {
			dbFavorite.setCreateTime(new Date());
			dbFavorite.setCreateUserId(1L);
			dbFavorite.setCreateUserName("faker");
			dbFavorite.setYn(1);
			dbFavoriteService.save(dbFavorite);
		}
		return DocDbResponseJson.ok();
	}
}

