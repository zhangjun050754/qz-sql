package com.qz.sql.api.parse;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.oracle.parser.OracleStatementParser;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.postgresql.parser.PGSQLStatementParser;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.sqlserver.parser.SQLServerStatementParser;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerSchemaStatVisitor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ParseSqlUtils {
    public Map<String, String> getTableAliasMap(String sql, String driverType) {
        try {
            if ("com.mysql.jdbc.Driver".equals(driverType)) {
                MySqlStatementParser parser = new MySqlStatementParser(sql);
                SQLStatement statement = parser.parseStatement();
                MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
                statement.accept(visitor);
                return getalis(visitor.getAliasMap());
            } else if ("org.postgresql.Driver".equals(driverType)) {
                PGSQLStatementParser pgsqlStatementParser = new PGSQLStatementParser(sql);
                SQLStatement statement = pgsqlStatementParser.parseStatement();
                PGSchemaStatVisitor visitor = new PGSchemaStatVisitor();
                statement.accept(visitor);
                return getalis(visitor.getAliasMap());
            } else if (("oracle.jdbc.driver.OracleDriver".equals(driverType))) {
                OracleStatementParser oracleStatementParser = new OracleStatementParser(sql);
                SQLStatement statement = oracleStatementParser.parseStatement();
                OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
                statement.accept(visitor);
                return getalis(visitor.getAliasMap());
            } else if ("net.sourceforge.jtds.jdbc.Driver".equals(driverType)) {
                SQLServerStatementParser sqlServerStatementParser = new SQLServerStatementParser(sql);
                SQLStatement statement = sqlServerStatementParser.parseStatement();
                SQLServerSchemaStatVisitor visitor = new SQLServerSchemaStatVisitor();
                statement.accept(visitor);
                return getalis(visitor.getAliasMap());
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public Map<String, String> getalis( Map<String, String> map){
        HashMap<String, String> newMap = new HashMap<>();
        if(map.size()>0){
            Set<String> strings = map.keySet();
            for(String tmp:strings){
                if(map.get(tmp) == null|| !tmp.equals(map.get(tmp))){
                    newMap.put(tmp,map.get(tmp));
                }
            }
            return newMap;
        }
        return newMap;
    }

    public static void main(String[] args) {
        ParseSqlUtils p =new ParseSqlUtils();
        Map<String, String> tableAliasMap = p.getTableAliasMap("select * from public.test", "org.postgresql.Driver");
        System.out.println();
    }
}
