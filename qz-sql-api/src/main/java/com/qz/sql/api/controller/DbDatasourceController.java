package com.qz.sql.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.qz.sql.editor.bean.DatabaseFactoryBean;
import com.qz.sql.editor.bean.DatabaseRegistrationBean;
import com.qz.sql.editor.configuration.DatasourceUtil;
import com.qz.sql.editor.entity.DbDatasource;
import com.qz.sql.editor.json.DocDbResponseJson;
import com.qz.sql.editor.json.ResponseJson;
import com.qz.sql.editor.service.DbDatasourceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 数据源控制器
 *
 * @author
 * @
 */
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/qz-sql/datasource")
public class DbDatasourceController {

	@Resource
	DatabaseRegistrationBean databaseRegistrationBean;
	@Resource
	DbDatasourceService dbDatasourceService;

	/**
	 * 获取数据源列表
	 * @return
	 */
	@PostMapping(value = "/list")
	public ResponseJson list() {
		QueryWrapper<DbDatasource> wrapper = new QueryWrapper<>();
		wrapper.eq("yn", 1);
		List<DbDatasource> datasourceList = dbDatasourceService.list(wrapper);
		for (DbDatasource dbDatasource : datasourceList) {
			dbDatasource.setSourcePassword("***");
		}
		return DocDbResponseJson.ok(datasourceList);
	}

	@PostMapping(value = "/test")
	public ResponseJson test(DbDatasource dbDatasource) {
		// 验证新的数据源
		try {
			// 获取原始密码
			if (Objects.equals("***", dbDatasource.getSourcePassword()) && dbDatasource.getId() != null) {
				DbDatasource dbDatasourceSel = dbDatasourceService.getById(dbDatasource.getId());
				if (dbDatasourceSel != null) {
					dbDatasource.setSourcePassword(dbDatasourceSel.getSourcePassword());
				}
			}
			DatabaseFactoryBean databaseFactoryBean = DatasourceUtil.createDatabaseFactoryBean(dbDatasource);
			if (databaseFactoryBean == null) {
				return DocDbResponseJson.warn("获取数据源失败，请检查配置是否正确");
			}
			databaseFactoryBean.getDataSource().close();
		} catch (Exception e) {
			e.printStackTrace();
			return DocDbResponseJson.warn("获取数据源失败，请检查配置是否正确");
		}
		return DocDbResponseJson.ok();
	}

	@PostMapping(value = "/update")
	public ResponseJson update(DbDatasource dbDatasource) {
		if (StringUtils.isBlank(dbDatasource.getName())) {
			return DocDbResponseJson.warn("名字必填");
		} else if (StringUtils.isBlank(dbDatasource.getDriverClassName())) {
			return DocDbResponseJson.warn("驱动类必选");
		} else if (StringUtils.isBlank(dbDatasource.getSourceUrl())) {
			return DocDbResponseJson.warn("地址必填");
		} else if (StringUtils.isBlank(dbDatasource.getSourceName())) {
			return DocDbResponseJson.warn("用户名必填");
		} else if (StringUtils.isBlank(dbDatasource.getSourcePassword())) {
			return DocDbResponseJson.warn("密码必填");
		}
		if (Objects.equals("***", dbDatasource.getSourcePassword())) {
			dbDatasource.setSourcePassword(null);
		}
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		// 这三项不需要修改
		dbDatasource.setCreateTime(ft.format(new Date()));
		dbDatasource.setCreateUserId(null);
		dbDatasource.setCreateUserName(null);
		Long sourceId = Optional.ofNullable(dbDatasource.getId()).orElse(0L);
		if (sourceId > 0) {
			dbDatasourceService.updateById(dbDatasource);
			// 关闭旧数据源
			databaseRegistrationBean.closeDatasource(dbDatasource.getId());
		} else {
			dbDatasource.setCreateTime(ft.format(new Date()));
			dbDatasource.setCreateUserId(1L);
			dbDatasource.setCreateUserName("faker");
			dbDatasource.setYn(1);
			dbDatasourceService.save(dbDatasource);
		}
		return DocDbResponseJson.ok();
	}
}

