package com.qz.sql.api;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;




@CrossOrigin
@SpringBootApplication
@ComponentScan("com.qz")
@MapperScan(basePackages = {"com.qz.sql.editor.mapper", "com.qz.sql.editor.mapper.mysql"})
public class Application {

	public static void main(String[] args)  {
		SpringApplication.run(Application.class, args);
	}
}