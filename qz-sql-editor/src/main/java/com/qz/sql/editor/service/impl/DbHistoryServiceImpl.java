package com.qz.sql.editor.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.qz.sql.editor.entity.DbHistory;
import com.qz.sql.editor.mapper.DbHistoryMapper;
import com.qz.sql.editor.service.DbHistoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *


 */
@Service
public class DbHistoryServiceImpl extends ServiceImpl<DbHistoryMapper, DbHistory> implements DbHistoryService {
	
	@Resource
	DbHistoryMapper dbHistoryMapper;
	
	@Override
	public void saveHistory(String content, Long datasourceId) {
//		DocUserDetails currentUser = DocUserUtil.getCurrentUser();
		DbHistory dbHistory = new DbHistory();
		dbHistory.setDatasourceId(datasourceId);
		dbHistory.setContent(content);
		dbHistory.setCreateTime(new Date());
//		dbHistory.setCreateUserId(currentUser.getUserId());
//		dbHistory.setCreateUserName(currentUser.getUsername());
		dbHistory.setYn(1);
		this.save(dbHistory);
		// 删除多余的数据
		dbHistoryMapper.deleteHistory();
	}
}
