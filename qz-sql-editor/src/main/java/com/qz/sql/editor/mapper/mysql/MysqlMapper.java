package com.qz.sql.editor.mapper.mysql;

import com.qz.sql.editor.dto.ColumnInfoDto;
import org.apache.ibatis.annotations.Param;

/**
 * mysql数据库的mapper持有对象
 * 


 */
public interface MysqlMapper {
	
	ColumnInfoDto getColumnInfo(@Param("dbName") String dbName, @Param("tableName") String tableName, @Param("columnName") String columnName);
}