package com.qz.sql.editor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qz.sql.editor.entity.DbFavorite;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *


 */
public interface DbFavoriteMapper extends BaseMapper<DbFavorite> {

}
