package com.qz.sql.editor.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qz.sql.editor.entity.DbHistory;

/**
 * <p>
 *  服务类
 * </p>
 *


 */
public interface DbHistoryService extends IService<DbHistory> {
	
	void saveHistory(String content, Long datasourceId);
	
}
