package com.qz.sql.editor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qz.sql.editor.entity.DbHistory;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *


 */
public interface DbHistoryMapper extends BaseMapper<DbHistory> {
	
	@Update("delete a from db_history a,(select id from db_history order by id desc limit 100, 1) b where a.id < b.id")
	void deleteHistory();
	
}
