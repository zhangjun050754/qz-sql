package com.qz.sql.editor.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qz.sql.editor.entity.DbFavorite;
import com.qz.sql.editor.mapper.DbFavoriteMapper;
import com.qz.sql.editor.service.DbFavoriteService;
import org.springframework.stereotype.Service;


@Service
public class DbFavoriteServiceImpl extends ServiceImpl<DbFavoriteMapper, DbFavorite> implements DbFavoriteService {

}