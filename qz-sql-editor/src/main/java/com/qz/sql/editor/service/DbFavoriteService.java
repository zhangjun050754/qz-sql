package com.qz.sql.editor.service;

import com.qz.sql.editor.entity.DbFavorite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *


 */
public interface DbFavoriteService extends IService<DbFavorite> {

}

