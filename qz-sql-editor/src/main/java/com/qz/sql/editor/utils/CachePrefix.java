package com.qz.sql.editor.utils;

public class CachePrefix {
	public static final String WIKI_LOCK_PAGE = "WIKI_LOCK_PAGE_";
	public static final String DB_EDITOR_DATA_CACHE = "DB_EDITOR_DATA_CACHE_";
}
