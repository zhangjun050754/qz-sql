package com.qz.sql.editor.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.qz.sql.editor.exception.ConfirmException;

import java.util.concurrent.atomic.AtomicLong;

public class DruidDataSourceUtil {
	
	private static AtomicLong nameId = new AtomicLong(0);
	
	public static DruidDataSource createDataSource(String driverClassName, String url, String username, String password) throws Exception {
		// 数据源配置
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setInitialSize(2);
		dataSource.setMinIdle(2);
		dataSource.setMaxActive(50);
		dataSource.setTestWhileIdle(true);
		dataSource.setTestOnBorrow(false);
		dataSource.setTestOnReturn(false);
		dataSource.setValidationQuery("select 1");
		dataSource.setMaxWait(3000);
		dataSource.setTimeBetweenEvictionRunsMillis(60000);
		dataSource.setMinEvictableIdleTimeMillis(3600000);
		dataSource.setConnectionErrorRetryAttempts(2);
		dataSource.setBreakAfterAcquireFailure(false);
		dataSource.setTimeBetweenConnectErrorMillis(180000);
		dataSource.setName("zyplayer-doc-db-" + nameId.incrementAndGet());
		if (url.contains("oracle")) {
			dataSource.setValidationQuery("select 1 from dual");
		}
		DruidPooledConnection connection = dataSource.getConnection(3000);
		if (connection == null) {
			throw new ConfirmException("尝试获取该数据源连接失败：" + url);
		}
		connection.recycle();
		return dataSource;
	}
}
