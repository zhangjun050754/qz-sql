package com.qz.sql.editor.dto;

public class DatabaseInfoDto {
	private String dbName;

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

}
