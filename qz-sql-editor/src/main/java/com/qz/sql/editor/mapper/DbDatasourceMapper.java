package com.qz.sql.editor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qz.sql.editor.entity.DbDatasource;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *


 */
public interface DbDatasourceMapper extends BaseMapper<DbDatasource> {

}
