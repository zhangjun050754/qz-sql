package com.qz.sql.editor.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.qz.sql.editor.entity.DbDatasource;
import com.qz.sql.editor.mapper.DbDatasourceMapper;
import com.qz.sql.editor.service.DbDatasourceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *


 */
@Service
public class DbDatasourceServiceImpl extends ServiceImpl<DbDatasourceMapper, DbDatasource> implements DbDatasourceService {

}
