package com.qz.sql.editor.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.qz.sql.editor.bean.DatabaseFactoryBean;
import com.qz.sql.editor.entity.DbDatasource;
import com.qz.sql.editor.interceptor.SqlLogInterceptor;
import com.qz.sql.editor.utils.DruidDataSourceUtil;
import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class DatasourceUtil {
	private static SqlLogInterceptor sqlLogInterceptor = new SqlLogInterceptor();
	
	public static DatabaseFactoryBean createDatabaseFactoryBean(DbDatasource dbDatasource) throws Exception {
		Resource[] resources = null;
		// 描述连接信息的对象
		DatabaseFactoryBean databaseFactoryBean = new DatabaseFactoryBean();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		String dbUrl = dbDatasource.getSourceUrl();
		if (dbUrl.contains("mysql")) {
			String[] urlParamArr = dbUrl.split("\\?");
			String[] urlDbNameArr = urlParamArr[0].split("/");
			if (urlDbNameArr.length >= 2) {
				databaseFactoryBean.setDbName(urlDbNameArr[urlDbNameArr.length - 1]);
			}
			databaseFactoryBean.setDatabaseProduct(DatabaseFactoryBean.DatabaseProduct.MYSQL);
			resources = resolver.getResources("classpath:mapper/mysql/*.xml");
		} else if (dbUrl.contains("sqlserver")) {
			String[] urlParamArr = dbUrl.split(";");
			for (String urlParam : urlParamArr) {
				String[] keyValArr = urlParam.split("=");
				if (keyValArr.length >= 2 && keyValArr[0].equalsIgnoreCase("DatabaseName")) {
					databaseFactoryBean.setDbName(keyValArr[1]);
				}
			}
			databaseFactoryBean.setDatabaseProduct(DatabaseFactoryBean.DatabaseProduct.SQLSERVER);
			resources = resolver.getResources("classpath:mapper/sqlserver/*.xml");
		} else if (dbUrl.contains("oracle")) {

			String[] urlParamArr = dbUrl.split("\\?")[0].split("@");
			String[] urlDbNameArr = urlParamArr[0].split("/");
			if (urlDbNameArr.length <= 1) {
				urlDbNameArr = urlParamArr[0].split(":");
			}
			databaseFactoryBean.setDbName(urlDbNameArr[urlDbNameArr.length - 1]);
			databaseFactoryBean.setDatabaseProduct(DatabaseFactoryBean.DatabaseProduct.ORACLE);
			resources = resolver.getResources("classpath:mapper/oracle/*.xml");
		} else if (dbUrl.contains("postgresql")){
			String[] urlParamArr = dbUrl.split("\\?");
			String[] urlDbNameArr = urlParamArr[0].split("/");
			if (urlDbNameArr.length >= 2) {
				databaseFactoryBean.setDbName(urlDbNameArr[urlDbNameArr.length - 1]);
			}
			databaseFactoryBean.setDatabaseProduct(DatabaseFactoryBean.DatabaseProduct.POSTGRESQL);
			resources = resolver.getResources("classpath:mapper/postgresql/*.xml");


		}
		if (resources == null) {
			return null;
		}
		// 数据源配置
		DruidDataSource dataSource = DruidDataSourceUtil.createDataSource(dbDatasource.getDriverClassName(), dbDatasource.getSourceUrl(), dbDatasource.getSourceName(), dbDatasource.getSourcePassword());
		// 创建sqlSessionTemplate
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
		sqlSessionFactoryBean.setMapperLocations(resources);
		sqlSessionFactoryBean.setPlugins(new Interceptor[]{sqlLogInterceptor});
		SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactoryBean.getObject());
		// 组装自定义的bean
		databaseFactoryBean.setId(dbDatasource.getId());
		databaseFactoryBean.setCnName(dbDatasource.getName());
		databaseFactoryBean.setDataSource(dataSource);
		databaseFactoryBean.setSqlSessionTemplate(sqlSessionTemplate);
		databaseFactoryBean.setUrl(dbUrl);
		return databaseFactoryBean;
	}
}
