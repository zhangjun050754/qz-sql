package com.qz.sql.editor.mapper.base;

import java.util.Map;

/**
 * 执行结果回调
 *


 */
public interface ResultHandler {
	
	void handleResult(Map<String, Object> resultMap);
	
}
