package com.qz.sql.editor.mapper.base;


import com.qz.sql.editor.dto.*;
import com.qz.sql.editor.vo.TableStatusVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据库的mapper持有对象接口
 *
 * @author 贾博
 * @since
 */
public interface BaseMapper {
	
	/**
	 * 获取库列表
	 *
	 * @return 数据库列表
	 * @author 贾博
	 * @since
	 */
	Map<String, String> getTableDdl(@Param("dbName") String dbName, @Param("tableName") String tableName);
	
	/**
	 * 获取库列表
	 *
	 * @return 数据库列表
	 * @author 贾博
	 * @since
	 */
	List<DatabaseInfoDto> getDatabaseList(@Param("dbName") String dbName);
	
	/**
	 * 获取表列表
	 *
	 * @param dbName 数据库名
	 * @return 数据库表列表
	 * @author 贾博
	 * @since
	 */
	List<TableInfoDto> getTableList(@Param("dbName") String dbName);
	
	/**
	 * 获取字段列表
	 *
	 * @param dbName    数据库名
	 * @param tableName 表名
	 * @return 字段列表
	 * @author 贾博
	 * @since
	 */
	List<TableColumnDescDto> getTableColumnList(@Param("dbName") String dbName, @Param("tableName") String tableName);
	
	/**
	 * 获取字段注释
	 *
	 * @param tableName 表名
	 * @return 表字段注释
	 * @author 贾博
	 * @since
	 */
	List<TableColumnDescDto> getTableColumnDescList(@Param("tableName") String tableName);
	
	/**
	 * 模糊搜索表和字段
	 *
	 * @param dbName     数据库名
	 * @param searchText 搜索内容
	 * @param searchTable
	 * @param searchColumn
	 * @return 表和字段信息
	 * @author 贾博
	 * @since
	 */
	List<QueryTableColumnDescDto> getTableAndColumnBySearch(@Param("dbName") String dbName, @Param("searchText") String searchText, @Param("searchTable") String searchTable, @Param("searchColumn") String searchColumn);
	
	/**
	 * 获取表注释
	 *
	 * @param tableName 可不传，传了只查询指定表的注释
	 * @return 表注释
	 * @author 贾博
	 * @since
	 */
	List<TableDescDto> getTableDescList(@Param("dbName") String dbName, @Param("tableName") String tableName);
	
	/**
	 * 增加表注释
	 *
	 * @param tableName 表名
	 * @param newDesc   新的注释
	 * @author 贾博
	 * @since
	 */
	void updateTableDesc(@Param("dbName") String dbName, @Param("tableName") String tableName, @Param("newDesc") String newDesc);
	
	/**
	 * 增加字段注释
	 *
	 * @param dbName     数据库名
	 * @param tableName  表名
	 * @param columnName 字段名
	 * @param newDesc    新的注释
	 * @param columnInfo 字段信息
	 * @author 贾博
	 * @since
	 */
	void updateTableColumnDesc(@Param("dbName") String dbName, @Param("tableName") String tableName,
                               @Param("columnName") String columnName, @Param("newDesc") String newDesc,
                               @Param("columnInfo") ColumnInfoDto columnInfo);
	
	/**
	 * 获取表基本信息
	 *
	 * @param dbName    数据库名
	 * @param tableName 表名
	 * @author 贾博
	 * @since
	 */
	TableStatusVo getTableStatus(@Param("dbName") String dbName, @Param("tableName") String tableName);
}
