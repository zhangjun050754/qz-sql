package com.qz.sql.editor.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qz.sql.editor.entity.DbDatasource;

/**
 * <p>
 *  服务类
 * </p>
 *


 */
public interface DbDatasourceService extends IService<DbDatasource> {

}
