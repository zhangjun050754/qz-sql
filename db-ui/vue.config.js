module.exports = {
	devServer: {
		hot: true,
		disableHostCheck: true,
		port: 8084,
		host: 'localhost'
	},
	publicPath: './',
	productionSourceMap: false
};
