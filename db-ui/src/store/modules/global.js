export default {
    namespaced: true,
    state: {
        pageTabNameMap: {},
        fullscreen: false,
        color: "background-color: white; color: black",
        font: 15
    },
    getters: {
        getPageTabNameMap (state) {
            return state.pageTabNameMap;
        },
        getColor (state) {
            return state.color;
        },
        getFont (state) {
            return state.font;
        }
    },
    mutations: {
        addTableName (state, item) {
            let sameObj = Object.assign({}, state.pageTabNameMap);
            sameObj[item.key] = item.val;
            state.pageTabNameMap = sameObj;
        },
        setFullscreen (state, val) {
            state.fullscreen = val;
        },
        setColor (state, val) {
            state.color = val;
            localStorage.setItem("color", val)
        },
        setFont (state, val) {
            state.font = val;
            localStorage.setItem("font", val)
        }
    }
}
