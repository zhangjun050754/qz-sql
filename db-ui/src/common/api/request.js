import axios from 'axios'
import vue from '../../main'

const service = axios.create({
	baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url process.env.APP_BASE_API
	timeout: 100000
});

service.interceptors.request.use(config => {
	return config
}, error => {
	// Do something with request error
	Promise.reject(error)
})

// 增加不需要验证结果的标记
const noValidate = {
	"/qz-sql/executor/execute": true,
	"/qz-sql/datasource/test": true,
};

service.interceptors.request.use(
	config => {
		config.needValidateResult = true;
		// 增加不需要验证结果的标记
		if (noValidate[config.url]) {
			config.needValidateResult = false;
		}
		return config
	},
	error => {
		console.log(error);
		return Promise.reject(error);
	}
);

service.interceptors.response.use(
	response => {
		if (response.data instanceof Blob) {
			return response.data
		}
		if (!!response.message) {
			vue.$message.error('请求错误：' + response.message);
		} else {
			if (response.data.errCode == 200) {
				return response.data;
			} else if (response.data.errCode == 400) {
				vue.$message.error('请先登录');
				var href = encodeURIComponent(window.location.href);
				window.location = process.env.VUE_APP_BASE_API + "#/user/login?redirect=" + href;
			} else if (response.data.errCode == 402) {
				vue.$router.push("/common/noAuth").catch(err => { err })
			} else {
				console.log("asdasdasd")
				vue.$message.error(response.data.errMsg || "未知错误");
			}
		}
		return Promise.reject('请求错误');
	},
	error => {
		console.log('err' + error);
		vue.$message.info('请求错误：' + error.message);
		return Promise.reject(error)
	}
);
export default service;
