import PageTableView from './components/layouts/PageTableView'
import TableInfo from './views/table/Info.vue'
import DatasourceManage from '@/views/data/DatasourceManage'
import TableDatabase from './views/table/Database.vue'
import test from './views/test/index.vue'
let routes = [
    {
        path: "/",
        redirect: "/test"
    }, {
        path: '/',
        name: 'Tab标签页',
        component: PageTableView,
        children: [
            { path: '/table/info', name: '表信息', component: TableInfo },
            { path: '/table/database', name: '库信息', component: TableDatabase }
            // { path: '/data/executor', name: 'SQL执行器', component: DataExecutor }
        ]
    },
    {
        path: '/manage',
        name: '数据源管理',
        component: DatasourceManage
    },
    {
        path: '/test',
        name: '数据源管理',
        component: test
    }
];

export default routes;
